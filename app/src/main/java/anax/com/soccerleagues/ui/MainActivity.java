package anax.com.soccerleagues.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.List;

import anax.com.soccerleagues.R;
import anax.com.soccerleagues.ui.base.AppFragment;
import anax.com.soccerleagues.ui.fragments.LeaguesFragment;
import anax.com.soccerleagues.ui.fragments.TeamsFragment;

public class MainActivity extends AppCompatActivity
        implements LeaguesFragment.LeagueRetrieveListener, TeamsFragment.TeamRetrieveListener {

    private static final int INTERNET_PERMISSIONS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        getPermissionInternet();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportFragmentManager().beginTransaction().add(R.id.container, LeaguesFragment.newInstance()).commit();
    }

    public void getPermissionInternet() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.INTERNET},
                    INTERNET_PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onBackPressed() {
        if (!onBackPressedUser()) super.onBackPressed();
    }

    public boolean onBackPressedUser() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof AppFragment && ((AppFragment) fragment).dispatchOnBackPressed()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == INTERNET_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Internet access permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Internet access permission denied", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onLeagueSelected(String leagueUrl, String leagueName) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, TeamsFragment.newInstance(leagueUrl, leagueName)).commit();
    }

    @Override
    public void onLeagueRetrieveFailed() {
        showDialog("Cannot retrieve league list ", "Retry", "Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LeaguesFragment.newInstance()).commit();
                } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                    finish();
                }
            }
        });
    }

    @Override
    public void OnTeamsRetrieveFailed(final String teamUrl, final String legue) {
        showDialog("Cannot retrieve teams for league " + legue, "Retry", "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, TeamsFragment.newInstance(teamUrl, legue)).commit();
                } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, LeaguesFragment.newInstance()).commit();
                }
            }
        });
    }

    @Override
    public void onReturnRequested() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, LeaguesFragment.newInstance()).commit();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
    }

    private void showDialog(String title, String ok, String cancel, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setPositiveButton(ok, listener);
        builder.setNegativeButton(cancel, listener);
        builder.show();
    }
}
