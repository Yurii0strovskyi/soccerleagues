package anax.com.soccerleagues.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import anax.com.soccerleagues.R;
import anax.com.soccerleagues.model.leagues.League;
import anax.com.soccerleagues.ui.base.AppFragment;
import anax.com.soccerleagues.utils.LeagueRetrieveTask;

/**
 * @author YuriiOstrovskyi on 4/19/16.
 */
public class LeaguesFragment extends AppFragment implements LeagueRetrieveTask.Callback {
    public interface LeagueRetrieveListener {
        void onLeagueSelected(String leagueUrl, String leagueName);
        void onLeagueRetrieveFailed();
    }
    private static final String URL = "http://api.football-data.org/v1/soccerseasons";

    private ProgressBar mProgressBar;

    private LeagueAdapter mLeagueAdapter = new LeagueAdapter();

    private LeagueRetrieveTask mTask = new LeagueRetrieveTask();

    public static LeaguesFragment newInstance() {
        return new LeaguesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTask.setCallback(this);
        mTask.execute(URL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_leagues, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        mProgressBar.setVisibility(View.VISIBLE);

        RecyclerView leagueRecyclerView = (RecyclerView) view.findViewById(R.id.league_list);
        leagueRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        leagueRecyclerView.setAdapter(mLeagueAdapter);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTask.setCallback(null);
        mTask.cancel(true);
    }

    @Override
    public void onRequestStarted() {
    }

    @Override
    public void onDataRetrieved(List<League> leagues) {
        mLeagueAdapter.setLeagues(leagues);
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError() {
        getCallback(LeagueRetrieveListener.class).onLeagueRetrieveFailed();
    }

    private class LeagueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView year;
        TextView matchDays;
        TextView currentMatchDay;


        public LeagueViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            year = (TextView) itemView.findViewById(R.id.year);
            matchDays = (TextView) itemView.findViewById(R.id.match_days);
            currentMatchDay = (TextView) itemView.findViewById(R.id.current_match_day);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            LeagueRetrieveListener listener = LeaguesFragment.this.getCallback(LeagueRetrieveListener.class);
            League league = mLeagueAdapter.getLeague(getLayoutPosition());
            listener.onLeagueSelected(league.getLinks().getTeams().getHref(), league.getLeague());
        }
    }

    private class LeagueAdapter extends RecyclerView.Adapter<LeagueViewHolder> {

        private List<League> mLeaguesList = new ArrayList<>();

        @Override
        public LeagueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.v_league_item, parent, false);
            return new LeagueViewHolder(view);
        }

        @Override
        public void onBindViewHolder(LeagueViewHolder holder, int position) {
            holder.name.setText(mLeaguesList.get(position).getCaption());
            holder.year.setText("Year : " + mLeaguesList.get(position).getYear());
            holder.matchDays.setText("Match Days: " + mLeaguesList.get(position).getNumberOfMatchdays());
            holder.currentMatchDay.setText("Current match day: " + mLeaguesList.get(position).getCurrentMatchday());
        }

        @Override
        public int getItemCount() {
            return mLeaguesList.size();
        }

        public League getLeague(int position) {
            return mLeaguesList.get(position);
        }

        public void setLeagues(List<League> leagueList) {
            mLeaguesList = leagueList;
            notifyDataSetChanged();
        }
    }

}
