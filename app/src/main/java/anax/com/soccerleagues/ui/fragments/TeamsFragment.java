package anax.com.soccerleagues.ui.fragments;

import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import anax.com.soccerleagues.R;
import anax.com.soccerleagues.model.teams.Team;
import anax.com.soccerleagues.ui.base.AppFragment;
import anax.com.soccerleagues.utils.TeamsRetrieveTask;
import anax.com.soccerleagues.utils.svg.SvgDecoder;
import anax.com.soccerleagues.utils.svg.SvgDrawableTranscoder;
import anax.com.soccerleagues.utils.svg.SvgSoftwareLayerSetter;

/**
 * @author YuriiOstrovskyi on 4/19/16.
 */
public class TeamsFragment extends AppFragment implements TeamsRetrieveTask.Callback {
    public interface TeamRetrieveListener {
        void OnTeamsRetrieveFailed(String teamUrl, String legue);
        void onReturnRequested();
    }

    public static final String TEAM_URL_TAG = "teamUrl";
    public static final String LEAGUE_NAME_TAG = "leagueName";

    private TeamsRetrieveTask mTask = new TeamsRetrieveTask();

    private Adapter mAdapter = new Adapter();

    private ProgressBar mProgressBar;

    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> mRequestBuilder;

    public static TeamsFragment newInstance(String teamUrl, String leagueName) {
        TeamsFragment fragment = new TeamsFragment();
        Bundle args = new Bundle();
        args.putString(TEAM_URL_TAG, teamUrl);
        args.putString(LEAGUE_NAME_TAG, leagueName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRequestBuilder = Glide.with(this)
                .using(Glide.buildStreamModelLoader(Uri.class, getContext()), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .placeholder(R.mipmap.ic_leagues_icon)
                .error(R.mipmap.error_image)
                .animate(android.R.anim.fade_in)
                .listener(new SvgSoftwareLayerSetter<Uri>());


        String teamsUrl = getArguments().getString(TEAM_URL_TAG);
        String leagueName = getArguments().getString(LEAGUE_NAME_TAG);
        if (!TextUtils.isEmpty(teamsUrl)) {
            mTask.setCallback(this);
            mTask.execute(teamsUrl);
        } else {
            getCallback(TeamRetrieveListener.class).OnTeamsRetrieveFailed(teamsUrl, leagueName);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTask.setCallback(null);
        mTask.cancel(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_teams, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        mProgressBar.setVisibility(View.VISIBLE);

        RecyclerView teamsRecyclerView = (RecyclerView) view.findViewById(R.id.teams_list);
        teamsRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        teamsRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onBackPressed() {
        getCallback(TeamRetrieveListener.class).onReturnRequested();
        return true;
    }

    @Override
    public void onRequestStarted() {
    }

    @Override
    public void onTeamsRetrieved(List<Team> teams) {
        mAdapter.setTeamsList(teams);
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError() {
        getCallback(TeamRetrieveListener.class).OnTeamsRetrieveFailed(
                getArguments().getString(TEAM_URL_TAG),
                getArguments().getString(LEAGUE_NAME_TAG));
    }

    private class TeamViewHolder extends RecyclerView.ViewHolder {

        TextView teamName;
        TextView budget;
        ImageView logo;

        public TeamViewHolder(View itemView) {
            super(itemView);

            teamName = (TextView) itemView.findViewById(R.id.name);
            budget = (TextView) itemView.findViewById(R.id.budget);
            logo = (ImageView) itemView.findViewById(R.id.logo);
            logo.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }

    private class Adapter extends RecyclerView.Adapter<TeamViewHolder> {

        List<Team> mTeamsList = new ArrayList<>();

        @Override
        public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TeamViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.v_team_item, parent, false));
        }

        @Override
        public void onBindViewHolder(TeamViewHolder holder, int position) {
            holder.teamName.setText(mTeamsList.get(position).getName());
            holder.budget.setText(mTeamsList.get(position).getSquadMarketValue());
            int height = getResources().getDimensionPixelSize(R.dimen.card_height);
            int width = getResources().getDisplayMetrics().widthPixels;
            mRequestBuilder.diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .load(Uri.parse(mTeamsList.get(position).getCrestUrl()))
                    .override(width, height)
                    .into(holder.logo);
        }

        @Override
        public int getItemCount() {
            return mTeamsList.size();
        }

        public void setTeamsList(List<Team> teamsList) {
            mTeamsList = teamsList;
            notifyDataSetChanged();
        }
    }
}
