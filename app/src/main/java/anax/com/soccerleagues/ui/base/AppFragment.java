package anax.com.soccerleagues.ui.base;

import android.support.v4.app.Fragment;

import java.util.List;

/**
 * @author YuriiOstrovskyi on 4/20/16.
 */
public class AppFragment extends Fragment {

    public <T>T getCallback(Class<T> cls) {
        Fragment fragment = getParentFragment();
        while(fragment != null) {
            if (cls.isInstance(fragment)) {
                return cls.cast(fragment);
            }
            fragment = getParentFragment();
        }
        return cls.cast(getActivity());
    }

    public boolean isActive() {
        return getActivity() != null && !isRemoving() && isVisible();
    }

    public boolean hasFinished() {
        return !isActive();
    }

    public final boolean dispatchOnBackPressed() {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof AppFragment && ((AppFragment) fragment).dispatchOnBackPressed()) {
                    return true;
                }
            }
        }

        if (onBackPressed()) {
            return true;
        }

        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            getChildFragmentManager().popBackStackImmediate();
            return true;
        }

        return false;
    }

    public boolean onBackPressed() {
        return false;
    }
}
