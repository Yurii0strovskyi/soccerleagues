package anax.com.soccerleagues.utils;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;

import anax.com.soccerleagues.model.leagues.League;

/**
 * @author YuriiOstrovskyi on 4/20/16.
 */
public class LeagueRetrieveTask extends AsyncTask<String, String, String> {
    public interface Callback {
        void onRequestStarted();
        void onDataRetrieved(List<League> leagues);
        void onError();
    }

    private Callback mCallback;

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onRequestStarted();
        }
    }

    @Override
    protected String doInBackground(@NonNull String... urls) {
        return NetUtils.executeGetRequest(urls[0]);
    }

    @Override
    protected void onPostExecute(String response) {
        if (!TextUtils.isEmpty(response)) {
            League[] leagues = NetUtils.parseJson(response, League[].class);
            if (mCallback != null) {
                mCallback.onDataRetrieved(Arrays.asList(leagues));
            }
        } else {
            if (mCallback != null) {
                mCallback.onError();
            }
        }
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }
}
