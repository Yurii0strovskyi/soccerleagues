package anax.com.soccerleagues.utils;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author YuriiOstrovskyi on 4/20/16.
 */
public final class NetUtils {
    public interface Callback<T> {
        void onRequestStarted();
        void onDataRetrieved(T leagues);
        void onError();
    }

    public static final String TAG = NetUtils.class.getSimpleName();

    public static final String AUTH_KEY = "X-Auth-Token";
    public static final String AUTH_TOKEN = "7588feff77364fbf90aa74f58158d211";

    private static Gson sGson = new Gson();

    private static ThreadLocal<OkHttpClient> sHttpClient = new ThreadLocal<OkHttpClient>() {
        @Override
        protected OkHttpClient initialValue() {
            return new OkHttpClient();
        }
    };

    public static String executeGetRequest(String url) {
        OkHttpClient client = sHttpClient.get();
        Request request = new Request.Builder()
                .addHeader(AUTH_KEY, AUTH_TOKEN)
                .url(url)
                .build();
        String responseBody = "";
        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
        } catch (IOException e) {
            Log.d(TAG, "IOException", e);
        }
        return responseBody;
    }

    public static <T> T parseJson(String json, Class<T> modelClass) {
        return sGson.fromJson(json, modelClass);
    }
}
