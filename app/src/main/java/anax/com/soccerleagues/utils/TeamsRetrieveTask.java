package anax.com.soccerleagues.utils;

import android.os.AsyncTask;
import android.text.TextUtils;

import java.util.List;

import anax.com.soccerleagues.model.teams.Team;
import anax.com.soccerleagues.model.teams.TeamList;

/**
 * @author YuriiOstrovskyi on 4/20/16.
 */
public class TeamsRetrieveTask extends AsyncTask<String, String, String> {
    public interface Callback {
        void onRequestStarted();
        void onTeamsRetrieved(List<Team> teams);
        void onError();
    }

    private Callback mCallback;

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onRequestStarted();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        return NetUtils.executeGetRequest(params[0]);
    }

    @Override
    protected void onPostExecute(String response) {
        if (!TextUtils.isEmpty(response)) {
            List<Team> teams = NetUtils.parseJson(response, TeamList.class).getTeams();
            if (mCallback != null) {
                mCallback.onTeamsRetrieved(teams);
            }
        } else {
            if (mCallback != null) {
                mCallback.onError();
            }
        }
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }
}
